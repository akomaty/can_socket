# CAN sockets program

This readme is made to explain the steps need to run the code. It should include all technical details and dependencies in a clear and concise way so that anyone will be able to re-run the code without problems.

1. Client-Server Relation 
2. Create virtual can interfaces using these steps

```sh
sudo apt install can-utils
modprobe can
modprobe can_raw
modprobe vcan
sudo ip link add dev vcan0 type vcan
sudo ip link set up vcan0
ip link show vcan0
<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
```
>>>>>>> e8933a7c1f24fe5cde35b013a762cf92850fa805

>>>>>>> 34e1470dc6b0003a9e147eab86d18026f3e3a222
3. adding can code
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

int
main(void)
{
	int s;
	int nbytes;
	struct sockaddr_can addr;
	struct can_frame frame;
	struct ifreq ifr;

	const char *ifname = "vcan0";

	if((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Error while opening socket");
		return -1;
	}

	strcpy(ifr.ifr_name, ifname);
	ioctl(s, SIOCGIFINDEX, &ifr);
	
	addr.can_family  = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	printf("%s at index %d\n", ifname, ifr.ifr_ifindex);

	if(bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Error in socket bind");
		return -2;
	}

	frame.can_id  = 0x123;
	frame.can_dlc = 2;
	frame.data[0] = 0x11;
	frame.data[1] = 0x22;

	nbytes = write(s, &frame, sizeof(struct can_frame));

	printf("Wrote %d bytes\n", nbytes);
	
	return 0;
}
```
4. put the code of client with code of Client, because of relation between client & server to allow sending between server & can
5. adding method send in code because it can socket work like TCP Socket , for sending data between can socket and server
6. define port as short and add for loop and increment port
7. adding a string for concatenating i(in for loop) with `vcan` to let client automatically sent to can socket
8. adding code of can socket in server to be sure of sending data from two-way 
9. to run code : 

```sh
gcc server.c -o server
gcc CANSocket.c -o can
run as first server(./server) then run can (./can)
```