#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h>  
#include <net/if.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>

int main(int argc, char const *argv[]) 
{ 
    	short PORT=1214; 
		char *Hello = "Hello from TCP"; 
    	int i=0;

   		char input[]="Hello from TCP Socket";
        int server_fd, new_socket, valread; 
        struct sockaddr_in address; 
        int opt = 1; 
        int addrlen = sizeof(address); 
        char buffer[1024] = {0}; 
    	
           
        // Creating socket file descriptor 
        if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
        { 
            perror("socket failed"); 
            exit(EXIT_FAILURE); 
        } 
        printf("%s\n", "Socket is connected");
        
        // Forcefully attaching socket to the port 8080 
        if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                      &opt, sizeof(opt))) 
        { 
            perror("setsockopt"); 
            exit(EXIT_FAILURE); 
        } 
        address.sin_family = AF_INET; 
        address.sin_addr.s_addr = INADDR_ANY; 
        address.sin_port = htons( PORT ); 
           
        // Forcefully attaching socket to the port 8080 
        if (bind(server_fd, (struct sockaddr *)&address,  
                                     sizeof(address))<0) 
        { 
            perror("bind failed"); 
            exit(EXIT_FAILURE); 
        } 
        printf("%s\n", "Socket bind Successfully");
        if (listen(server_fd, 3) < 0) 
        { 
            perror("listen"); 
            exit(EXIT_FAILURE); 
        } 
        printf("%s\n", "Socket is listening");
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address,  
                           (socklen_t*)&addrlen))<0) 
        { 
            perror("accept"); 
            exit(EXIT_FAILURE); 
        } 
        send(new_socket, Hello, strlen(Hello), 0 ); 

 		valread = read( new_socket , buffer, 1024); 

        printf("%s\n",buffer ); 
       /*
        valread = read( new_socket , buffer, 1024); 

        printf("%s\n",buffer ); 

        send(new_socket , input , strlen(input) , 0 ); 
    	

        printf("message received from client\n"); 
*/
        char s1[20];
        sprintf( s1, "%s%d", "vcan", i );
        /******************************Defining CAN SOCKET*************************/
        int s;
    	int nbytes;
    	struct sockaddr_can addr;
    	struct can_frame frame;
    	struct ifreq ifr;

    	const char *ifname = s1;

    	if((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
    		perror("Error while opening socket");
    		return -1;
    	}

    	strcpy(ifr.ifr_name, ifname);
    	ioctl(s, SIOCGIFINDEX, &ifr);
    	
    	addr.can_family  = AF_CAN;
    	addr.can_ifindex = ifr.ifr_ifindex;

    	//printf("%s at index %d\n", ifname, ifr.ifr_ifindex);

    	if(bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    		perror("Error in socket bind");
    		return -2;
    	}

    	frame.can_id  = 0x123;
    	frame.can_dlc = 2;
    	frame.data[0] = 0x11;
    	frame.data[1] = 0x22;

    	//nbytes = write(s, input, sizeof(struct can_frame));

    	//printf("Wrote %d bytes\n", nbytes);
    	//printf("message received\n"); 
    	

        send(s , input , strlen(input) , 0 ); 
        printf("%s\n", "message is sent to virtual can interface");
/*
        PORT =PORT +1;

    }else{
        printf("%s\n", "Insert a value not null");
        return 0;
        }
//}
*/
return 0;
} 
